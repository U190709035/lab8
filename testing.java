import shapes2d.Circle;
import shapes2d.Square;
import shapes3d.Cube;
import shapes3d.Cylinder;

public class testing {
    public static void main(String[] args) {
        Circle Circle = new Circle(5);
        System.out.println("circle area " + Circle.area());
        System.out.println("side of "+ Circle.toString());
        System.out.println("------------------------------------");

        Square Square = new Square(9);
        System.out.println("square area " + Square.area());
        System.out.println("side of " + Square.toString());
        System.out.println("-------------------------------------");


        Cube Cube = new Cube(4);
        System.out.println("cube area " + Cube.area());
        System.out.println("cube volume  " + Cube.volume());
        System.out.println("side of " + Cube.toString());
        System.out.println("---------------------------------------");


        Cylinder Cylinder = new Cylinder(3, 6);
        System.out.println("cylinder area " + Cylinder.area());
        System.out.println("cylinder volume " + Cylinder.volume());
        System.out.println("side of " + Cylinder.toString());

    }
}

