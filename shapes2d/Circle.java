package shapes2d;

public class Circle {

    public double radius;




    public Circle(double radius) {
        super();
        this.radius = radius;

    }


    public double area(){

        return Math.PI * radius * radius;


    }


    public String toString() {
        return "Circle" + " rad" + "("+radius+")" ;

    }





}